﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

using Repository.Pattern.DataContext;
using Repository.Pattern.Infrastructure;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;

#endregion

namespace Repository.Pattern.Ef6
{
    using Microsoft.Practices.ServiceLocation;

    public class UnitOfWork : IUnitOfWorkAsync
    {
        #region Private Fields

        private IDataContextAsync dataContext;
        private bool disposed;
        private ObjectContext objectContext;
        private DbTransaction transaction;
        private Dictionary<string, dynamic> repositories;

        #endregion Private Fields

        #region Constuctor

        public UnitOfWork(IDataContextAsync dataContext)
        {
            this.dataContext = dataContext;
            this.repositories = new Dictionary<string, dynamic>();
        }

        #endregion Constuctor/Dispose

        #region Dispose
        //https://msdn.microsoft.com/library/ms244737.aspx

        // Dispose() calls Dispose(true)
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~UnitOfWork()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }
        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    // free other managed objects that implement
                    // IDisposable only

                    try
                    {
                        if (this.objectContext != null)
                        {
                            if (this.objectContext.Connection.State == ConnectionState.Open)
                                this.objectContext.Connection.Close();

                            this.objectContext.Dispose();
                            this.objectContext = null;
                        }
                        if (this.dataContext != null)
                        {
                            this.dataContext.Dispose();
                            this.dataContext = null;
                        }
                    }
                    catch (ObjectDisposedException)
                    {
                        // do nothing, the objectContext has already been disposed
                    }

                    if (repositories != null)
                        this.repositories = null;
                }

                this.disposed = true;
            }            
        }

        #endregion

        public int SaveChanges()
        {
            return this.dataContext.SaveChanges();
        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : class, IObjectState
        {
            if (ServiceLocator.IsLocationProviderSet)
            {
                return ServiceLocator.Current.GetInstance<IRepository<TEntity>>();
            }

            return RepositoryAsync<TEntity>();
        }

        public Task<int> SaveChangesAsync()
        {
            return this.dataContext.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return this.dataContext.SaveChangesAsync(cancellationToken);
        }

        public IRepositoryAsync<TEntity> RepositoryAsync<TEntity>() where TEntity : class, IObjectState
        {
            if (ServiceLocator.IsLocationProviderSet)
            {
                return ServiceLocator.Current.GetInstance<IRepositoryAsync<TEntity>>();
            }

            if (this.repositories == null)
            {
                this.repositories = new Dictionary<string, dynamic>();
            }

            var type = typeof(TEntity).Name;

            if (this.repositories.ContainsKey(type))
            {
                return (IRepositoryAsync<TEntity>)this.repositories[type];
            }

            var repositoryType = typeof(Repository<>);

            this.repositories.Add(type, Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity)), this.dataContext, this));

            return this.repositories[type];
        }

        #region Unit of Work Transactions

        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
        {
            this.objectContext = ((IObjectContextAdapter) this.dataContext).ObjectContext;
            if (this.objectContext.Connection.State != ConnectionState.Open)
            {
                this.objectContext.Connection.Open();
            }

            this.transaction = this.objectContext.Connection.BeginTransaction(isolationLevel);
        }

        public bool Commit()
        {
            this.transaction.Commit();
            return true;
        }

        public void Rollback()
        {
            this.transaction.Rollback();
            this.dataContext.SyncObjectsStatePostCommit();
        }

        #endregion
    }
}